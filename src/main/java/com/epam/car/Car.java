package com.epam.car;

import lombok.Data;

@Data
public class Car {
    private int speed;
    private String name;

    public Car(int speed, String name) {
        this.speed = speed;
        this.name = name;
    }
}
